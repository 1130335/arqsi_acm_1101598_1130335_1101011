﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ClassLibrary.Model
{
    public enum TipoAnuncio
    {
        Compra,
        Venda,
        Permuta,
        Aluguer
    }

    public class Anuncio
    {
        public int AnuncioID { get; set; }
         
        [Required]
        [Display(Name = "Utilizador")]
        public ApplicationUser UtilizadorCriador { get; set; }

        [Required]
        [Display(Name = "Imovel")]
        public Imovel Imovel { get; set; }

        [Required]
        [Display(Name = "Tipo de Anuncio")]
        public TipoAnuncio Tipo { get; set; }

        [Display(Name = "Mediador")]
        public ApplicationUser MediadorResponsavel { get; set; }
        
    }

    public class Imovel
    {
        public int ImovelID { get; set; }

        [Required]
        [Display(Name = "Preço")]
        public float Preco { get; set; }

        [Required]
        [Display(Name = "Area")]
        public float Area { get; set; }

        [Required]
        [Display(Name = "Tipo de Imovel")]
        public TipoImovel TipoImovel { get; set; }

        [Required]
        [Display(Name = "Localização")]
        public Localizacao Localização { get; set; }

        [Display(Name = "Fotos")]
        public Fotos Fotos { get; set; }

        public virtual ICollection<Anuncio> Anuncios { get; set; }
    }

    public class TipoImovel
    {
        public int TipoImovelID { get; set; }

        [Required]
        [Display(Name = "Descricao")]
        public string Descricao { get; set; }

        [Display(Name = "Tipo Imovel Adicional")]
        public TipoImovel TipoAdicional { get; set; }

        public virtual ICollection<Imovel> Imoveis { get; set; }
    }

    public class Localizacao
    {
        public int LocalizacaoID { get; set; }

        [Required]
        [Display(Name = "Morada")]
        public string Morada { get; set; }

        [Display(Name = "Coordenadas")] 
        public string Coordenadas { get; set; }
    }

    public class Fotos
    {
        public int FotosID { get; set; }

        [Display(Name = "URL")]
        public string URL { get; set; }
    }

    public class Alerta
    {
        public int AlertaID { get; set; }

        [Required]
        [Display(Name = "Utilizador Criador")]
        public ApplicationUser UtilizadorCriador { get; set; }

        [Required]
        [Display(Name = "Parametros")]
        public string Parametros { get; set; }
    }
}
