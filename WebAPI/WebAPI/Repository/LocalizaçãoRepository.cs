﻿using AutoMapper;
using ClassLibrary.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using WebAPI.Models;

namespace WebAPI.Repository
{
    public interface ILocalizacaoRepository
    {
        LocalizacaoBindingModel GetLocalizacaoById(int localizaçãoId);
        IEnumerable<LocalizacaoBindingModel> GetAllLocalizacaos();
        int CreateLocalizacao(LocalizacaoBindingModel localização);
        bool UpdateLocalizacao(int localizaçãoId, LocalizacaoBindingModel localização);
        bool DeleteLocalizacao(int localizaçãoId);
    }

    public class LocalizacaoRepository : ILocalizacaoRepository
    {
        private readonly UnitOfWork _unitOfWork;

        public LocalizacaoRepository()
        {
            _unitOfWork = new UnitOfWork();
        }

        public int CreateLocalizacao(LocalizacaoBindingModel localizaçãoModel)
        {
            using (var scope = new TransactionScope())
            {
                var localização = new Localizacao
                {
                    Coordenadas = localizaçãoModel.Coordenadas,
                    Morada = localizaçãoModel.Morada
                };
                _unitOfWork.LocalizacaoRepository.Insert(localização);
                _unitOfWork.Save();
                scope.Complete();

                return localização.LocalizacaoID;
            }
        }

        public bool DeleteLocalizacao(int localizaçãoId)
        {
            var success = false;
            if (localizaçãoId > 0)
            {
                using (var scope = new TransactionScope())
                {
                    var localização = _unitOfWork.LocalizacaoRepository.GetByID(localizaçãoId);
                    if (localização != null)
                    {
                        _unitOfWork.LocalizacaoRepository.Delete(localização);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }

        public IEnumerable<LocalizacaoBindingModel> GetAllLocalizacaos()
        {
            var localizaçãos = _unitOfWork.LocalizacaoRepository.GetAll().ToList();
            if (localizaçãos.Any())
            {
                Mapper.CreateMap<Localizacao, LocalizacaoBindingModel>();
                var localizaçãosModel = Mapper.Map<List<Localizacao>, List<LocalizacaoBindingModel>>(localizaçãos);
                return localizaçãosModel;
            }
            return null;
        }

        public LocalizacaoBindingModel GetLocalizacaoById(int localizaçãoId)
        {
            var localização = _unitOfWork.LocalizacaoRepository.GetByID(localizaçãoId);
            if (localização != null)
            {
                Mapper.CreateMap<Localizacao, LocalizacaoBindingModel>();
                var localizaçãoModel = Mapper.Map<Localizacao, LocalizacaoBindingModel>(localização);
                return localizaçãoModel;
            }
            return null;
        }

        public bool UpdateLocalizacao(int localizaçãoId, LocalizacaoBindingModel localização)
        {
            var success = false;
            if (localização != null)
            {
                using (var scope = new TransactionScope())
                {
                    var localizaçãoLocal = _unitOfWork.LocalizacaoRepository.GetByID(localizaçãoId);
                    if (localizaçãoLocal != null)
                    {
                        localizaçãoLocal.Coordenadas = localização.Coordenadas;
                        localizaçãoLocal.Morada = localização.Morada;
                        _unitOfWork.LocalizacaoRepository.Update(localizaçãoLocal);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }
    }
}