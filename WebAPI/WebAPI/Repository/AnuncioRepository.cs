﻿using AutoMapper;
using ClassLibrary.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using WebAPI.Models;

namespace WebAPI.Repository
{

    public interface IAnuncioRepository
    {
        AnuncioBindingModel GetAnuncioById(int anuncioId);
        IEnumerable<AnuncioBindingModel> GetAllAnuncios();
        int CreateAnuncio(AnuncioBindingModel anuncio);
        bool UpdateAnuncio(int anuncioId, AnuncioBindingModel anuncio);
        bool DeleteAnuncio(int anuncioId);
    }

    public class AnuncioRepository : IAnuncioRepository
    {
        private readonly UnitOfWork _unitOfWork;

        public AnuncioRepository()
        {
            _unitOfWork = new UnitOfWork();
        }

        public int CreateAnuncio(AnuncioBindingModel anuncioModel)
        {
            using (var scope = new TransactionScope())
            {
                var anuncio = new Anuncio
                {
                    Imovel = anuncioModel.Imovel,
                    MediadorResponsavel = anuncioModel.MediadorResponsavel,
                    Tipo = anuncioModel.Tipo,
                    UtilizadorCriador = anuncioModel.UtilizadorCriador
                };
                _unitOfWork.AnuncioRepository.Insert(anuncio);
                _unitOfWork.Save();
                scope.Complete();

                return anuncio.AnuncioID;
            }
        }

        public bool DeleteAnuncio(int anuncioId)
        {
            var success = false;
            if (anuncioId > 0)
            {
                using (var scope = new TransactionScope())
                {
                    var anuncio = _unitOfWork.AnuncioRepository.GetByID(anuncioId);
                    if (anuncio != null)
                    {
                        _unitOfWork.AnuncioRepository.Delete(anuncio);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }

        public IEnumerable<AnuncioBindingModel> GetAllAnuncios()
        {
            var anuncios = _unitOfWork.AnuncioRepository.GetAll().ToList();
            if (anuncios.Any())
            {
                Mapper.CreateMap<Anuncio, AnuncioBindingModel>();
                var anunciosModel = Mapper.Map<List<Anuncio>, List<AnuncioBindingModel>>(anuncios);
                return anunciosModel;
            }
            return null;
        }

        public AnuncioBindingModel GetAnuncioById(int anuncioId)
        {
            var anuncio = _unitOfWork.AnuncioRepository.GetByID(anuncioId);
            if (anuncio != null)
            {
                Mapper.CreateMap<Anuncio, AnuncioBindingModel>();
                var anuncioModel = Mapper.Map<Anuncio, AnuncioBindingModel>(anuncio);
                return anuncioModel;
            }
            return null;
        }

        public bool UpdateAnuncio(int anuncioId, AnuncioBindingModel anuncio)
        {
            var success = false;
            if (anuncio != null)
            {
                using (var scope = new TransactionScope())
                {
                    var anuncioLocal = _unitOfWork.AnuncioRepository.GetByID(anuncioId);
                    if (anuncioLocal != null)
                    {
                        anuncioLocal.Imovel = anuncio.Imovel;
                        anuncioLocal.MediadorResponsavel = anuncio.MediadorResponsavel;
                        anuncioLocal.Tipo = anuncio.Tipo;
                        anuncioLocal.UtilizadorCriador = anuncio.UtilizadorCriador;
                        _unitOfWork.AnuncioRepository.Update(anuncioLocal);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }
    }
}