﻿using AutoMapper;
using ClassLibrary.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using WebAPI.Models;

namespace WebAPI.Repository
{
    public interface IFotosRepository
    {
        FotosBindingModel GetFotosById(int fotoId);
        IEnumerable<FotosBindingModel> GetAllFotoss();
        int CreateFotos(FotosBindingModel foto);
        bool UpdateFotos(int fotoId, FotosBindingModel foto);
        bool DeleteFotos(int fotoId);
    }

    public class FotosRepository : IFotosRepository
    {
        private readonly UnitOfWork _unitOfWork;

        public FotosRepository()
        {
            _unitOfWork = new UnitOfWork();
        }

        public int CreateFotos(FotosBindingModel fotoModel)
        {
            using (var scope = new TransactionScope())
            {
                var foto = new Fotos
                {
                    URL = fotoModel.URL
                };
                _unitOfWork.FotosRepository.Insert(foto);
                _unitOfWork.Save();
                scope.Complete();

                return foto.FotosID;
            }
        }

        public bool DeleteFotos(int fotoId)
        {
            var success = false;
            if (fotoId > 0)
            {
                using (var scope = new TransactionScope())
                {
                    var foto = _unitOfWork.FotosRepository.GetByID(fotoId);
                    if (foto != null)
                    {
                        _unitOfWork.FotosRepository.Delete(foto);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }

        public IEnumerable<FotosBindingModel> GetAllFotoss()
        {
            var fotos = _unitOfWork.FotosRepository.GetAll().ToList();
            if (fotos.Any())
            {
                Mapper.CreateMap<Fotos, FotosBindingModel>();
                var fotosModel = Mapper.Map<List<Fotos>, List<FotosBindingModel>>(fotos);
                return fotosModel;
            }
            return null;
        }

        public FotosBindingModel GetFotosById(int fotoId)
        {
            var foto = _unitOfWork.FotosRepository.GetByID(fotoId);
            if (foto != null)
            {
                Mapper.CreateMap<Fotos, FotosBindingModel>();
                var fotoModel = Mapper.Map<Fotos, FotosBindingModel>(foto);
                return fotoModel;
            }
            return null;
        }

        public bool UpdateFotos(int fotoId, FotosBindingModel foto)
        {
            var success = false;
            if (foto != null)
            {
                using (var scope = new TransactionScope())
                {
                    var fotoLocal = _unitOfWork.FotosRepository.GetByID(fotoId);
                    if (fotoLocal != null)
                    {
                        fotoLocal.URL = foto.URL;
                        _unitOfWork.FotosRepository.Update(fotoLocal);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }
    }
}