﻿using AutoMapper;
using ClassLibrary.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using WebAPI.Models;

namespace WebAPI.Repository
{
    public interface IImovelRepository
    {
        ImovelBindingModel GetImovelById(int imovelId);
        IEnumerable<ImovelBindingModel> GetAllImovels();
        int CreateImovel(ImovelBindingModel imovel);
        bool UpdateImovel(int imovelId, ImovelBindingModel imovel);
        bool DeleteImovel(int imovelId);
    }

    public class ImovelRepository : IImovelRepository
    {
        private readonly UnitOfWork _unitOfWork;

        public ImovelRepository()
        {
            _unitOfWork = new UnitOfWork();
        }

        public int CreateImovel(ImovelBindingModel imovelModel)
        {
            using (var scope = new TransactionScope())
            {
                var imovel = new Imovel
                {
                    Anuncios = imovelModel.Anuncios,
                    Area = imovelModel.Area,
                    Fotos = imovelModel.Fotos,
                    Localização = imovelModel.Localização,
                    Preco = imovelModel.Preco,
                    TipoImovel = imovelModel.TipoImovel
                };
                _unitOfWork.ImovelRepository.Insert(imovel);
                _unitOfWork.Save();
                scope.Complete();

                return imovel.ImovelID;
            }
        }

        public bool DeleteImovel(int imovelId)
        {
            var success = false;
            if (imovelId > 0)
            {
                using (var scope = new TransactionScope())
                {
                    var imovel = _unitOfWork.ImovelRepository.GetByID(imovelId);
                    if (imovel != null)
                    {
                        _unitOfWork.ImovelRepository.Delete(imovel);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }

        public IEnumerable<ImovelBindingModel> GetAllImovels()
        {
            var imovels = _unitOfWork.ImovelRepository.GetAll().ToList();
            if (imovels.Any())
            {
                Mapper.CreateMap<Imovel, ImovelBindingModel>();
                var imovelsModel = Mapper.Map<List<Imovel>, List<ImovelBindingModel>>(imovels);
                return imovelsModel;
            }
            return null;
        }

        public ImovelBindingModel GetImovelById(int imovelId)
        {
            var imovel = _unitOfWork.ImovelRepository.GetByID(imovelId);
            if (imovel != null)
            {
                Mapper.CreateMap<Imovel, ImovelBindingModel>();
                var imovelModel = Mapper.Map<Imovel, ImovelBindingModel>(imovel);
                return imovelModel;
            }
            return null;
        }

        public bool UpdateImovel(int imovelId, ImovelBindingModel imovel)
        {
            var success = false;
            if (imovel != null)
            {
                using (var scope = new TransactionScope())
                {
                    var imovelLocal = _unitOfWork.ImovelRepository.GetByID(imovelId);
                    if (imovelLocal != null)
                    {
                        imovelLocal.Anuncios = imovel.Anuncios;
                        imovelLocal.Area = imovel.Area;
                        imovelLocal.Fotos = imovel.Fotos;
                        imovelLocal.Localização = imovel.Localização;
                        imovelLocal.Preco = imovel.Preco;
                        imovelLocal.TipoImovel = imovel.TipoImovel;
                        _unitOfWork.ImovelRepository.Update(imovelLocal);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }
    }
}