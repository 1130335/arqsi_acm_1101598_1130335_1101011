﻿using AutoMapper;
using ClassLibrary.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using WebAPI.Models;

namespace WebAPI.Repository
{
    public interface ITipoImovelRepository
    {
        TipoImovelBindingModel GetTipoImovelById(int tipoImovelId);
        IEnumerable<TipoImovelBindingModel> GetAllTipoImovels();
        int CreateTipoImovel(TipoImovelBindingModel tipoImovel);
        bool UpdateTipoImovel(int tipoImovelId, TipoImovelBindingModel tipoImovel);
        bool DeleteTipoImovel(int tipoImovelId);
    }

    public class TipoImovelRepository : ITipoImovelRepository
    {
        private readonly UnitOfWork _unitOfWork;

        public TipoImovelRepository()
        {
            _unitOfWork = new UnitOfWork();
        }

        public int CreateTipoImovel(TipoImovelBindingModel tipoImovelModel)
        {
            using (var scope = new TransactionScope())
            {
                var tipoImovel = new TipoImovel
                {
                    Descricao = tipoImovelModel.Descricao,
                    TipoAdicional = tipoImovelModel.TipoAdicional,
                    Imoveis = tipoImovelModel.Imoveis
                };
                _unitOfWork.TipoImovelRepository.Insert(tipoImovel);
                _unitOfWork.Save();
                scope.Complete();

                return tipoImovel.TipoImovelID;
            }
        }

        public bool DeleteTipoImovel(int tipoImovelId)
        {
            var success = false;
            if (tipoImovelId > 0)
            {
                using (var scope = new TransactionScope())
                {
                    var tipoImovel = _unitOfWork.TipoImovelRepository.GetByID(tipoImovelId);
                    if (tipoImovel != null)
                    {
                        _unitOfWork.TipoImovelRepository.Delete(tipoImovel);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }

        public IEnumerable<TipoImovelBindingModel> GetAllTipoImovels()
        {
            var tipoImovels = _unitOfWork.TipoImovelRepository.GetAll().ToList();
            if (tipoImovels.Any())
            {
                Mapper.CreateMap<TipoImovel, TipoImovelBindingModel>();
                var tipoImovelsModel = Mapper.Map<List<TipoImovel>, List<TipoImovelBindingModel>>(tipoImovels);
                return tipoImovelsModel;
            }
            return null;
        }

        public TipoImovelBindingModel GetTipoImovelById(int tipoImovelId)
        {
            var tipoImovel = _unitOfWork.TipoImovelRepository.GetByID(tipoImovelId);
            if (tipoImovel != null)
            {
                Mapper.CreateMap<TipoImovel, TipoImovelBindingModel>();
                var tipoImovelModel = Mapper.Map<TipoImovel, TipoImovelBindingModel>(tipoImovel);
                return tipoImovelModel;
            }
            return null;
        }

        public bool UpdateTipoImovel(int tipoImovelId, TipoImovelBindingModel tipoImovel)
        {
            var success = false;
            if (tipoImovel != null)
            {
                using (var scope = new TransactionScope())
                {
                    var tipoImovelLocal = _unitOfWork.TipoImovelRepository.GetByID(tipoImovelId);
                    if (tipoImovelLocal != null)
                    {
                        tipoImovelLocal.Descricao = tipoImovel.Descricao;
                        tipoImovelLocal.TipoAdicional = tipoImovel.TipoAdicional;
                        tipoImovelLocal.Imoveis = tipoImovel.Imoveis;
                        _unitOfWork.TipoImovelRepository.Update(tipoImovelLocal);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }
    }
}