﻿using ClassLibrary.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Web;
using WebAPI.Models;

namespace WebAPI.Repository
{
    /// <summary>
    /// Unit of Work class responsible for DB transactions
    /// </summary>
    public class UnitOfWork : IDisposable
    {
        #region Private member variables...

        private ApplicationDbContext _context = null;
        private GenericRepository<Anuncio> _anuncioRepository;
        private GenericRepository<Imovel> _imovelRepository;
        private GenericRepository<TipoImovel> _tipoImovelRepository;
        private GenericRepository<Alerta> _alertaRepository;
        private GenericRepository<Fotos> _fotosRepository;
        private GenericRepository<Localizacao> _localizacaoRepository;

        #endregion

        public UnitOfWork()
        {
            _context = new ApplicationDbContext();
        }

        #region Public Repository Creation properties...

        /// <summary>
        /// Get/Set Property for anuncio repository.
        /// </summary>
        public GenericRepository<Anuncio> AnuncioRepository
        {
            get
            {
                if (this._anuncioRepository == null)
                    this._anuncioRepository = new GenericRepository<Anuncio>(_context);
                return _anuncioRepository;
            }
        }

        /// <summary>
        /// Get/Set Property for alerta repository.
        /// </summary>
        public GenericRepository<Alerta> AlertaRepository
        {
            get
            {
                if (this._alertaRepository == null)
                    this._alertaRepository = new GenericRepository<Alerta>(_context);
                return _alertaRepository;
            }
        }

        /// <summary>
        /// Get/Set Property for tipo de imovel repository.
        /// </summary>
        public GenericRepository<TipoImovel> TipoImovelRepository
        {
            get
            {
                if (this._tipoImovelRepository == null)
                    this._tipoImovelRepository = new GenericRepository<TipoImovel>(_context);
                return _tipoImovelRepository;
            }
        }

        /// <summary>
        /// Get/Set Property for imovel repository.
        /// </summary>
        public GenericRepository<Imovel> ImovelRepository
        {
            get
            {
                if (this._imovelRepository == null)
                    this._imovelRepository = new GenericRepository<Imovel>(_context);
                return _imovelRepository;
            }
        }

        /// <summary>
        /// Get/Set Property for fotos repository.
        /// </summary>
        public GenericRepository<Fotos> FotosRepository
        {
            get
            {
                if (this._fotosRepository == null)
                    this._fotosRepository = new GenericRepository<Fotos>(_context);
                return _fotosRepository;
            }
        }

        /// <summary>
        /// Get/Set Property for localizacao repository.
        /// </summary>
        public GenericRepository<Localizacao> LocalizacaoRepository
        {
            get
            {
                if (this._localizacaoRepository == null)
                    this._localizacaoRepository = new GenericRepository<Localizacao>(_context);
                return _localizacaoRepository;
            }
        }
        #endregion

        #region Public member methods...
        /// <summary>
        /// Save method.
        /// </summary>
        public void Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {

                var outputLines = new List<string>();
                foreach (var eve in e.EntityValidationErrors)
                {
                    outputLines.Add(string.Format(
                        "{0}: Entity of type \"{1}\" in state \"{2}\" has the following validation errors:", DateTime.Now,
                        eve.Entry.Entity.GetType().Name, eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        outputLines.Add(string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage));
                    }
                }
                System.IO.File.AppendAllLines(@"C:\errors.txt", outputLines);

                throw e;
            }

        }

        #endregion

        #region Implementing IDiosposable...

        #region private dispose variable declaration...
        private bool disposed = false;
        #endregion

        /// <summary>
        /// Protected Virtual Dispose method
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    Debug.WriteLine("UnitOfWork is being disposed");
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Dispose method
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}