﻿using AutoMapper;
using ClassLibrary.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using WebAPI.Models;

namespace WebAPI.Repository
{
    public interface IAlertaRepository
    {
        AlertaBindingModel GetAlertaById(int alertaId);
        IEnumerable<AlertaBindingModel> GetAllAlertas();
        int CreateAlerta(AlertaBindingModel alerta);
        bool UpdateAlerta(int alertaId, AlertaBindingModel alerta);
        bool DeleteAlerta(int alertaId);
    }

    public class AlertaRepository : IAlertaRepository
    {
        private readonly UnitOfWork _unitOfWork;

        public AlertaRepository()
        {
            _unitOfWork = new UnitOfWork();
        }

        public int CreateAlerta(AlertaBindingModel alertaModel)
        {
            using (var scope = new TransactionScope())
            {
                var alerta = new Alerta
                {
                    Parametros = alertaModel.Parametros,
                    UtilizadorCriador = alertaModel.UtilizadorCriador
                };
                _unitOfWork.AlertaRepository.Insert(alerta);
                _unitOfWork.Save();
                scope.Complete();

                return alerta.AlertaID;
            }
        }

        public bool DeleteAlerta(int alertaId)
        {
            var success = false;
            if (alertaId > 0)
            {
                using (var scope = new TransactionScope())
                {
                    var alerta = _unitOfWork.AlertaRepository.GetByID(alertaId);
                    if (alerta != null)
                    {
                        _unitOfWork.AlertaRepository.Delete(alerta);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }

        public IEnumerable<AlertaBindingModel> GetAllAlertas()
        {
            var alertas = _unitOfWork.AlertaRepository.GetAll().ToList();
            if (alertas.Any())
            {
                Mapper.CreateMap<Alerta, AlertaBindingModel>();
                var alertasModel = Mapper.Map<List<Alerta>, List<AlertaBindingModel>>(alertas);
                return alertasModel;
            }
            return null;
        }

        public AlertaBindingModel GetAlertaById(int alertaId)
        {
            var alerta = _unitOfWork.AlertaRepository.GetByID(alertaId);
            if (alerta != null)
            {
                Mapper.CreateMap<Alerta, AlertaBindingModel>();
                var alertaModel = Mapper.Map<Alerta, AlertaBindingModel>(alerta);
                return alertaModel;
            }
            return null;
        }

        public bool UpdateAlerta(int alertaId, AlertaBindingModel alerta)
        {
            var success = false;
            if (alerta != null)
            {
                using (var scope = new TransactionScope())
                {
                    var alertaLocal = _unitOfWork.AlertaRepository.GetByID(alertaId);
                    if (alertaLocal != null)
                    {
                        alertaLocal.Parametros = alerta.Parametros;
                        alertaLocal.UtilizadorCriador = alerta.UtilizadorCriador;
                        _unitOfWork.AlertaRepository.Update(alertaLocal);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }
    }
}