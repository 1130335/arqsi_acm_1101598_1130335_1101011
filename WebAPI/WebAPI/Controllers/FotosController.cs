﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ClassLibrary.Model;
using WebAPI.Models;
using WebAPI.Repository;

namespace WebAPI.Controllers
{
    [Authorize(Roles = "Cliente")]
    public class FotosController : ApiController
    {
        private readonly IFotosRepository _FotosRepository;

        #region Public Constructor

        public FotosController()
        {
            _FotosRepository = new FotosRepository();
        }

        #endregion

        public HttpResponseMessage Get()
        {
            var fotos = _FotosRepository.GetAllFotoss();
            if (fotos != null)
            {
                var fotosEntities = fotos as List<FotosBindingModel> ?? fotos.ToList();
                if (fotosEntities.Any())
                    return Request.CreateResponse(HttpStatusCode.OK, fotosEntities);
            }
            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Fotos não encontradas");
        }

        public HttpResponseMessage Get(int id)
        {
            var foto = _FotosRepository.GetFotosById(id);
            if (foto != null)
                return Request.CreateResponse(HttpStatusCode.OK, foto);
            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Não existe nenhuma foto com id dado");
        }

        public int Post([FromBody] FotosBindingModel fotoEntity)
        {
            return _FotosRepository.CreateFotos(fotoEntity);
        }

        public bool Put(int id, [FromBody]FotosBindingModel fotoEntity)
        {
            if (id > 0)
            {
                return _FotosRepository.UpdateFotos(id, fotoEntity);
            }
            return false;
        }

        public bool Delete(int id)
        {
            if (id > 0)
                return _FotosRepository.DeleteFotos(id);
            return false;
        }
    }
}