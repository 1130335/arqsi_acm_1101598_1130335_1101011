﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ClassLibrary.Model;
using WebAPI.Models;
using WebAPI.Repository;

namespace WebAPI.Controllers
{
    [Authorize(Roles = "Cliente")]
    public class LocalizacaosController : ApiController
    {
        private readonly ILocalizacaoRepository _LocalizacaoRepository;

        #region Public Constructor

        public LocalizacaosController()
        {
            _LocalizacaoRepository = new LocalizacaoRepository();
        }

        #endregion

        public HttpResponseMessage Get()
        {
            var localizacaos = _LocalizacaoRepository.GetAllLocalizacaos();
            if (localizacaos != null)
            {
                var localizacaosEntities = localizacaos as List<LocalizacaoBindingModel> ?? localizacaos.ToList();
                if (localizacaosEntities.Any())
                    return Request.CreateResponse(HttpStatusCode.OK, localizacaosEntities);
            }
            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Localizacoes não encontradas");
        }

        public HttpResponseMessage Get(int id)
        {
            var localizacao = _LocalizacaoRepository.GetLocalizacaoById(id);
            if (localizacao != null)
                return Request.CreateResponse(HttpStatusCode.OK, localizacao);
            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Não existe nenhuma localizacao com id dado");
        }

        public int Post([FromBody] LocalizacaoBindingModel localizacaoEntity)
        {
            return _LocalizacaoRepository.CreateLocalizacao(localizacaoEntity);
        }

        public bool Put(int id, [FromBody]LocalizacaoBindingModel localizacaoEntity)
        {
            if (id > 0)
            {
                return _LocalizacaoRepository.UpdateLocalizacao(id, localizacaoEntity);
            }
            return false;
        }

        public bool Delete(int id)
        {
            if (id > 0)
                return _LocalizacaoRepository.DeleteLocalizacao(id);
            return false;
        }
    }
}