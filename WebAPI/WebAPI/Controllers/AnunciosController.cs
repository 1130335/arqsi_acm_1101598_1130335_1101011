﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ClassLibrary.Model;
using WebAPI.Models;
using WebAPI.Repository;

namespace WebAPI.Controllers
{
    [Authorize(Roles = "Cliente")]
    public class AnunciosController : ApiController
    {
        private readonly IAnuncioRepository _AnuncioRepository;

        #region Public Constructor

        public AnunciosController()
        {
            _AnuncioRepository = new AnuncioRepository();
        }

        #endregion

        public HttpResponseMessage Get()
        {
            var anuncios = _AnuncioRepository.GetAllAnuncios();
            if (anuncios != null)
            {
                var anunciosEntities = anuncios as List<AnuncioBindingModel> ?? anuncios.ToList();
                if (anunciosEntities.Any())
                    return Request.CreateResponse(HttpStatusCode.OK, anunciosEntities);
            }
            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Anuncios não encontrados");
        }

        public HttpResponseMessage Get(int id)
        {
            var anuncio = _AnuncioRepository.GetAnuncioById(id);
            if (anuncio != null)
                return Request.CreateResponse(HttpStatusCode.OK, anuncio);
            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Não existe nenhum anuncio com id dado");
        }

        public int Post([FromBody] AnuncioBindingModel anuncioEntity)
        {
            return _AnuncioRepository.CreateAnuncio(anuncioEntity);
        }

        public bool Put(int id, [FromBody]AnuncioBindingModel anuncioEntity)
        {
            if (id > 0)
            {
                return _AnuncioRepository.UpdateAnuncio(id, anuncioEntity);
            }
            return false;
        }

        public bool Delete(int id)
        {
            if (id > 0)
                return _AnuncioRepository.DeleteAnuncio(id);
            return false;
        }
    }
}