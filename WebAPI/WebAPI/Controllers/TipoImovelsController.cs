﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ClassLibrary.Model;
using WebAPI.Models;
using WebAPI.Repository;

namespace WebAPI.Controllers
{
    public class TipoImovelsController : ApiController
    {
        private readonly ITipoImovelRepository _TipoImovelRepository;

        #region Public Constructor

        public TipoImovelsController()
        {
            _TipoImovelRepository = new TipoImovelRepository();
        }

        #endregion

        [Authorize(Roles = "Cliente")]
        public HttpResponseMessage Get()
        {
            var tipoImovels = _TipoImovelRepository.GetAllTipoImovels();
            if (tipoImovels != null)
            {
                var tipoImovelsEntities = tipoImovels as List<TipoImovelBindingModel> ?? tipoImovels.ToList();
                if (tipoImovelsEntities.Any())
                    return Request.CreateResponse(HttpStatusCode.OK, tipoImovelsEntities);
            }
            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "TipoImovels não encontrados");
        }

        [Authorize(Roles = "Cliente")]
        public HttpResponseMessage Get(int id)
        {
            var tipoImovel = _TipoImovelRepository.GetTipoImovelById(id);
            if (tipoImovel != null)
                return Request.CreateResponse(HttpStatusCode.OK, tipoImovel);
            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Não existe nenhum tipoImovel com id dado");
        }

        public int Post([FromBody] TipoImovelBindingModel tipoImovelEntity)
        {
            return _TipoImovelRepository.CreateTipoImovel(tipoImovelEntity);
        }

        public bool Put(int id, [FromBody]TipoImovelBindingModel tipoImovelEntity)
        {
            if (id > 0)
            {
                return _TipoImovelRepository.UpdateTipoImovel(id, tipoImovelEntity);
            }
            return false;
        }

        public bool Delete(int id)
        {
            if (id > 0)
                return _TipoImovelRepository.DeleteTipoImovel(id);
            return false;
        }
    }
}