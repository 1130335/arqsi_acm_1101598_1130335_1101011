﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ClassLibrary.Model;
using WebAPI.Models;
using WebAPI.Repository;

namespace WebAPI.Controllers
{
    [Authorize(Roles = "Cliente")]
    public class ImovelsController : ApiController
    {
        private readonly IImovelRepository _ImovelRepository;

        #region Public Constructor

        public ImovelsController()
        {
            _ImovelRepository = new ImovelRepository();
        }

        #endregion

        public HttpResponseMessage Get()
        {
            var imovels = _ImovelRepository.GetAllImovels();
            if (imovels != null)
            {
                var imovelsEntities = imovels as List<ImovelBindingModel> ?? imovels.ToList();
                if (imovelsEntities.Any())
                    return Request.CreateResponse(HttpStatusCode.OK, imovelsEntities);
            }
            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Imoveis não encontrados");
        }

        public HttpResponseMessage Get(int id)
        {
            var imovel = _ImovelRepository.GetImovelById(id);
            if (imovel != null)
                return Request.CreateResponse(HttpStatusCode.OK, imovel);
            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Não existe nenhum imovel com id dado");
        }

        public int Post([FromBody] ImovelBindingModel imovelEntity)
        {
            return _ImovelRepository.CreateImovel(imovelEntity);
        }

        public bool Put(int id, [FromBody]ImovelBindingModel imovelEntity)
        {
            if (id > 0)
            {
                return _ImovelRepository.UpdateImovel(id, imovelEntity);
            }
            return false;
        }

        public bool Delete(int id)
        {
            if (id > 0)
                return _ImovelRepository.DeleteImovel(id);
            return false;
        }
    }
}