﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Models;
using WebAPI.Repository;

namespace WebAPI.Controllers
{
    [Authorize(Roles = "Cliente")]
    public class AlertasController : ApiController
    {
        private readonly IAlertaRepository _AlertaRepository;

        #region Public Constructor

        public AlertasController()
        {
            _AlertaRepository = new AlertaRepository();
        }

        #endregion

        public HttpResponseMessage Get()
        {
            var alertas = _AlertaRepository.GetAllAlertas();
            if (alertas != null)
            {
                var alertasEntities = alertas as List<AlertaBindingModel> ?? alertas.ToList();
                if (alertasEntities.Any())
                    return Request.CreateResponse(HttpStatusCode.OK, alertasEntities);
            }
            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Alertas não encontrados");
        }

        public HttpResponseMessage Get(int id)
        {
            var alerta = _AlertaRepository.GetAlertaById(id);
            if (alerta != null)
                return Request.CreateResponse(HttpStatusCode.OK, alerta);
            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Não existe nenhum alerta com id dado");
        }

        public int Post([FromBody] AlertaBindingModel alertaEntity)
        {
            return _AlertaRepository.CreateAlerta(alertaEntity);
        }

        public bool Put(int id, [FromBody]AlertaBindingModel alertaEntity)
        {
            if (id > 0)
            {
                return _AlertaRepository.UpdateAlerta(id, alertaEntity);
            }
            return false;
        }

        public bool Delete(int id)
        {
            if (id > 0)
                return _AlertaRepository.DeleteAlerta(id);
            return false;
        }
    }
}
