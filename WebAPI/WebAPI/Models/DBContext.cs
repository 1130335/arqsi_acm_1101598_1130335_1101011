﻿using Microsoft.AspNet.Identity.EntityFramework;
using ClassLibrary.Model;
using System.Data.Entity;

namespace WebAPI.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }
        
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<Fotos> Fotos { get; set; }
        public DbSet<Localizacao> Localizacoes { get; set; }
        public DbSet<TipoImovel> TiposImoveis { get; set; }
        public DbSet<Imovel> Imoveis { get; set; }
        public DbSet<Anuncio> Anuncios { get; set; }
        public DbSet<Alerta> Alertas { get; set; }
    }
}