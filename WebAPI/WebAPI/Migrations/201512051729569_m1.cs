namespace WebAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class m1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Alertas",
                c => new
                    {
                        AlertaID = c.Int(nullable: false, identity: true),
                        Parametros = c.String(nullable: false),
                        UtilizadorCriador_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.AlertaID)
                .ForeignKey("dbo.AspNetUsers", t => t.UtilizadorCriador_Id, cascadeDelete: true)
                .Index(t => t.UtilizadorCriador_Id);
            
            CreateTable(
                "dbo.Anuncios",
                c => new
                    {
                        AnuncioID = c.Int(nullable: false, identity: true),
                        Tipo = c.Int(nullable: false),
                        Imovel_ImovelID = c.Int(nullable: false),
                        MediadorResponsavel_Id = c.String(maxLength: 128),
                        UtilizadorCriador_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.AnuncioID)
                .ForeignKey("dbo.Imovels", t => t.Imovel_ImovelID, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.MediadorResponsavel_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UtilizadorCriador_Id, cascadeDelete: true)
                .Index(t => t.Imovel_ImovelID)
                .Index(t => t.MediadorResponsavel_Id)
                .Index(t => t.UtilizadorCriador_Id);
            
            CreateTable(
                "dbo.Imovels",
                c => new
                    {
                        ImovelID = c.Int(nullable: false, identity: true),
                        Preco = c.Single(nullable: false),
                        Area = c.Single(nullable: false),
                        Fotos_FotosID = c.Int(),
                        Localização_LocalizacaoID = c.Int(nullable: false),
                        TipoImovel_TipoImovelID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ImovelID)
                .ForeignKey("dbo.Fotos", t => t.Fotos_FotosID)
                .ForeignKey("dbo.Localizacaos", t => t.Localização_LocalizacaoID, cascadeDelete: true)
                .ForeignKey("dbo.TipoImovels", t => t.TipoImovel_TipoImovelID, cascadeDelete: true)
                .Index(t => t.Fotos_FotosID)
                .Index(t => t.Localização_LocalizacaoID)
                .Index(t => t.TipoImovel_TipoImovelID);
            
            CreateTable(
                "dbo.Fotos",
                c => new
                    {
                        FotosID = c.Int(nullable: false, identity: true),
                        URL = c.String(),
                    })
                .PrimaryKey(t => t.FotosID);
            
            CreateTable(
                "dbo.Localizacaos",
                c => new
                    {
                        LocalizacaoID = c.Int(nullable: false, identity: true),
                        Morada = c.String(nullable: false),
                        Coordenadas = c.String(),
                    })
                .PrimaryKey(t => t.LocalizacaoID);
            
            CreateTable(
                "dbo.TipoImovels",
                c => new
                    {
                        TipoImovelID = c.Int(nullable: false, identity: true),
                        Descricao = c.String(nullable: false),
                        TipoAdicional_TipoImovelID = c.Int(),
                    })
                .PrimaryKey(t => t.TipoImovelID)
                .ForeignKey("dbo.TipoImovels", t => t.TipoAdicional_TipoImovelID)
                .Index(t => t.TipoAdicional_TipoImovelID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Anuncios", "UtilizadorCriador_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Anuncios", "MediadorResponsavel_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Anuncios", "Imovel_ImovelID", "dbo.Imovels");
            DropForeignKey("dbo.Imovels", "TipoImovel_TipoImovelID", "dbo.TipoImovels");
            DropForeignKey("dbo.TipoImovels", "TipoAdicional_TipoImovelID", "dbo.TipoImovels");
            DropForeignKey("dbo.Imovels", "Localização_LocalizacaoID", "dbo.Localizacaos");
            DropForeignKey("dbo.Imovels", "Fotos_FotosID", "dbo.Fotos");
            DropForeignKey("dbo.Alertas", "UtilizadorCriador_Id", "dbo.AspNetUsers");
            DropIndex("dbo.TipoImovels", new[] { "TipoAdicional_TipoImovelID" });
            DropIndex("dbo.Imovels", new[] { "TipoImovel_TipoImovelID" });
            DropIndex("dbo.Imovels", new[] { "Localização_LocalizacaoID" });
            DropIndex("dbo.Imovels", new[] { "Fotos_FotosID" });
            DropIndex("dbo.Anuncios", new[] { "UtilizadorCriador_Id" });
            DropIndex("dbo.Anuncios", new[] { "MediadorResponsavel_Id" });
            DropIndex("dbo.Anuncios", new[] { "Imovel_ImovelID" });
            DropIndex("dbo.Alertas", new[] { "UtilizadorCriador_Id" });
            DropTable("dbo.TipoImovels");
            DropTable("dbo.Localizacaos");
            DropTable("dbo.Fotos");
            DropTable("dbo.Imovels");
            DropTable("dbo.Anuncios");
            DropTable("dbo.Alertas");
        }
    }
}
