namespace ARQSI_IT2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Logins",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Username = c.String(nullable: false),
                        Password = c.String(nullable: false),
                        RememberMe = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Utilizadors", "Role_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.Utilizadors", "Role_Id");
            AddForeignKey("dbo.Utilizadors", "Role_Id", "dbo.AspNetRoles", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Utilizadors", "Role_Id", "dbo.AspNetRoles");
            DropIndex("dbo.Utilizadors", new[] { "Role_Id" });
            DropColumn("dbo.Utilizadors", "Role_Id");
            DropTable("dbo.Logins");
        }
    }
}
