﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ARQSI_IT2.Startup))]
namespace ARQSI_IT2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
