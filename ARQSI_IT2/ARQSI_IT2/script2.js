/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function preencheLayout(principal) {
    getFacetasAJAX();
    var form = document.createElement('form');
    var table = document.createElement('table');
    table.setAttribute('id', 'Main');
    table.setAttribute('align', 'center');

    var td_search = document.createElement('td');
    td_search.setAttribute('id', 'search_opts');
    td_search.setAttribute('valign', 'top');

    var div_filters = document.createElement('div');
    div_filters.setAttribute('id', 'div_filters');

    td_search.appendChild(div_filters);
    table.appendChild(td_search);
    form.appendChild(table);

    var td_style = document.createElement('td');
    td_style.setAttribute('vertical-align', 'top');
    table.appendChild(td_style);

    var div_results = document.createElement('div');
    div_results.setAttribute('id', 'div_searchresults');
    td_style.appendChild(div_results);

    var tb_results = document.createElement('table');
    tb_results.setAttribute('id', 'search_results');
    tb_results.setAttribute('class', 'tabelaresultados');
    div_results.appendChild(tb_results);

    var first_row = document.createElement('tr');
    first_row.setAttribute('id', 'firstrow');
    tb_results.appendChild(first_row);

    var t_body = document.createElement('tbody');
    t_body.setAttribute('id', 'search_results_body');
    tb_results.appendChild(t_body);

    var element = document.getElementById(principal);
    element.appendChild(form);
}
//------------------------------------------------------------------------------------
//var para object AJAX
var xmlHttpObj;

var facs = [];
var valorTipoFaceta = [];

//Tipos de faceta
var tiposfacetas = new Array();
//Colunas do tipo continuas
var colunas_continuas = new Array();


//faz o pedido a API e retorna as Facetas existentes
function getFacetasAJAX()
{
    xmlHttpObj = new XMLHttpRequest();
    if (xmlHttpObj)
    {
        xmlHttpObj.onreadystatechange = getFacetasXML;
        xmlHttpObj.open("GET", "http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/facetas.php", true);
        xmlHttpObj.send(null);
    }
}

//função que trata a resposta do pedido das facetas XML
function getFacetasXML()
{
    if (xmlHttpObj.readyState == 4 && xmlHttpObj.status == 200)
    {
        var xml = xmlHttpObj.responseXML;
        facetasXML = xml.getElementsByTagName("faceta");

        facetas = [];
        for (i = 0; i < facetasXML.length; i++)
        {
            textNode = facetasXML[i].childNodes[0];
            facetas[i] = textNode.nodeValue;
        }

        obterTipoFacetas(facetas);
        addfilter(facetas);

    }
}

// Faz o pedido a API e retorna os valores das Facetas
function getValoresFacetaAJAX(nomefaceta)
{
    divname = nomefaceta;
    xmlHttpObj = new XMLHttpRequest();
    if (xmlHttpObj)
    {
        xmlHttpObj.onreadystatechange = getValoresFacetaJSON;

        xmlHttpObj.open("GET", "http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/valoresFaceta.php?faceta=" + nomefaceta, true);
        xmlHttpObj.send(null);
    }
}

//recebe e trata valores facetas
function getValoresFacetaJSON()
{

    if (xmlHttpObj.readyState === 4 && xmlHttpObj.status === 200)
    {
        var jsonText = xmlHttpObj.responseText;
        var valoresFaceta = JSON.parse(jsonText);

        preencheValoresFaceta(valoresFaceta);
    }
}


//Função cria layout valores facetas usando DOM ----  FILHOS-FACETAS
function preencheValoresFaceta(valoresFaceta)
{
    var div_faceta = document.getElementById("div_options_" + divname);

    //Cria element blockquote (valores das facetas a aparecer com um tab
    var blockquote = document.createElement("blockquote");
    div_faceta.appendChild(blockquote);

    if (div_faceta.getAttribute('discreto_faceta') == 'discreto')
    {

        for (j = 0; j < valoresFaceta.length; j++)
        {

            var label = document.createElement("label");

            var description = document.createTextNode(valoresFaceta[j]);

            var br = document.createElement("br");

            //cria e adiciona input checkbox para valores das facetas utilizando DOM

            var inputfacetas = document.createElement('input');
            inputfacetas.type = 'checkbox';
            inputfacetas.align = 'left';
            inputfacetas.name = valoresFaceta[j];
            inputfacetas.id = valoresFaceta[j];
            inputfacetas.value = valoresFaceta[j];
            label.appendChild(inputfacetas);
            label.appendChild(description);
            label.appendChild(br);


            label.onchange = actualiza_procura;

            blockquote.appendChild(label);

        }
    }
    else {
        colunas_continuas.push(divname);
        var label_min = document.createElement("label");
        var description_min = document.createTextNode(divname + " Min");
        var br = document.createElement("br");
        var label_max = document.createElement("label");
        var description_max = document.createTextNode(divname + " Máx");



        var min = document.createElement('input');
        min.type = 'text';
        min.align = 'right';
        min.name = divname + "MIN";
        min.id = divname + "_min";
        min.value = 0;
        label_min.appendChild(min);
        label_min.appendChild(description_min);
        label_min.appendChild(br);

        var max = document.createElement('input');
        max.type = 'text';
        max.align = 'right';
        max.name = divname + "MAX";
        max.id = divname + "_max";
        max.value = 50000;
        label_max.appendChild(max);
        label_max.appendChild(description_max);
        label_max.appendChild(br);

        label_min.onchange = actualiza_procura;
        label_max.onchange = actualiza_procura;
        blockquote.appendChild(label_min);
        blockquote.appendChild(label_max)

    }
}


//função para esconder/mostrar valores das facetas ao clicar no botão, e para fazer a chamada AJAX para os valores das facetas.
function valoresfacetas(nomefaceta)
{
    var facetadiv = document.getElementById("div_options_" + nomefaceta);
 
    //----------CODIGO PARA ESCONDER DIV AO CLICAR BOTAO  WIP---------------------------
    if (facetadiv != undefined && facetadiv != null)
    {

        if (facetadiv.style.display !== 'none')
        {
            facetadiv.style.display = 'none';
        }
        else {
            facetadiv.style.display = 'block';
        }
    }
    if (!facetadiv.hasChildNodes())
    {
        getValoresFacetaAJAX(nomefaceta);
    }


}
//----------------Função que permite criar o layout para as opções de pesquisa, assim como a tabeça de resultados-------------------------------------------
function addfilter(facetas) {

    var div_filters = document.getElementById("div_filters");
    limpeza("div_filters");

//------ Criar Botões para cada faceta
    var faceta;
    for (i = 0; i < facetas.length; i++)
    {
        faceta = facetas[i];
        if (faceta.toString() !== "fotos") {
            var bt_facetas = document.createElement('input');
            bt_facetas.type = 'button';
            bt_facetas.name = "bt_" + faceta;
            bt_facetas.id = "bt_" + faceta;
            bt_facetas.value = faceta;
            bt_facetas.className = "btn";


            bt_facetas.onclick = (function () {
                var cfaceta = faceta;
                return function () {
                    valoresfacetas(cfaceta);
                }
            })();

            document.getElementById("div_filters").appendChild(bt_facetas);

            var div_options = document.createElement('div');
            div_options.name = "div_options_" + faceta;
            div_options.id = "div_options_" + faceta;
            div_options.setAttribute("nome_faceta", faceta);
            div_options.setAttribute("tipo_faceta", tiposfacetas[i].tipo);
            div_options.setAttribute("discreto_faceta", tiposfacetas[i].discreto);
            div_options.style.display = "";

            document.getElementById("div_filters").appendChild(div_options);
        }
    }
    document.getElementById("div_filters").style.display = "block";


    //---------------------- Tabela de Resultados-------------------------------------------------
    limpeza("firstrow");

    var first_row = document.getElementById("firstrow"); //primeira linha da tabela resultados - Titulos com nomes das Facetas
    for (i = 0; i < facetas.length; i++)
    {
        faceta = facetas[i];
        //TRATAR AS STRING FACETAS - _ por espaço
        var title_faceta = faceta.replace("_", " ");

        var titles_th = document.createElement('TH');
        titles_th.setAttribute("nome_faceta", faceta);
        var titles_text = document.createTextNode(title_faceta);
        titles_th.appendChild(titles_text);

        first_row.appendChild(titles_th);
    }


    document.getElementById("div_filters").style.display = "block";


}
//-----------------faz o pedido a API e retorna os tipos de variaveis das Facetas existentes-------------------------------------------
//
function obterTipoFacetas(facetas)
{
    for (i = 0; i < facetas.length; i++)
    {
        var facetai = facetas[i];
        getTipoVariavelFacetaAJAX(facetai);
    }
}

function getTipoVariavelFacetaAJAX(faceta)
{
    xmlHttpObj = new XMLHttpRequest();
    if (xmlHttpObj)
    {
        xmlHttpObj.onreadystatechange = getTipoVariavelFacetaJSON;

        xmlHttpObj.open("GET", "http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/tipoFaceta.php?faceta=" + faceta, false);
        xmlHttpObj.send(null);
    }
}

function getTipoVariavelFacetaJSON()
{

    if (xmlHttpObj.readyState == 4 && xmlHttpObj.status == 200)
    {
        var jsonText = xmlHttpObj.responseText;
        valorTipoFaceta = JSON.parse(jsonText);


        tiposfacetas.push(valorTipoFaceta);

    }
}


//--------------------------------------------------------------------------------------------------------------------------------------


//faz o pedido a API e retorna os valores das Facetas existentes
function getanuncioAJAX(search_params)
{
    xmlHttpObj = new XMLHttpRequest();
    if (xmlHttpObj)
    {
        xmlHttpObj.onreadystatechange = getanuncioJSON;

        xmlHttpObj.open("GET", "http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/imoveis.php?" + search_params, true);
        xmlHttpObj.send(null);
    }
}


//recebe e trata os valores das facetas
function getanuncioJSON()
{

    if (xmlHttpObj.readyState == 4 && xmlHttpObj.status == 200)
    {
        var jsonText = xmlHttpObj.responseText;
        var valorAnuncio = JSON.parse(jsonText);
        preencheResultados(valorAnuncio);
    }
}


// função que preenche a tabela de resultados com os valores da procura
function preencheResultados(valorAnuncio) {

    //valorTipoFacetas(facetas);
    // limpar tbody
    var tabelares = document.getElementById("search_results_body");
    limpeza("search_results_body");

//    var minimos = new Array();
//    var maximos = new Array();
//    for(var j=0;j<colunas_continuas.length;j++){
//        var tmpvalmin= document.getElementById(colunas_continuas[j]+"_min").value;
//        minimos.push(tmpvalmin);
//        var tmpvalmax= document.getElementById(colunas_continuas[j]+"_max").value;
//        maximos.push(tmpvalmax);
//    }
//    
    // Preencher tabela
    for (var i = 0; i < valorAnuncio.length; i++)
    {
        var n_row = document.createElement('TR');
        var anuncio = valorAnuncio[i];
        
        var flag_valido=true;
        //excluir anuncios que nao correspondem aos valores inseridos nas caixas de texto
        for(var k=0;k<colunas_continuas.length;k++){
            var val_coluna=parseInt(anuncio[colunas_continuas[k]]);
            var val_min=parseInt(document.getElementById(colunas_continuas[k]+"_min").value);
            var val_max=parseInt(document.getElementById(colunas_continuas[k]+"_max").value);
           
            if(val_coluna<val_min || val_coluna>val_max ){
                flag_valido=false;
            }
        }

        //Obter nome Colunas|atributo api
        var trows = document.getElementById("firstrow");
        var tables_cols = trows.getElementsByTagName("th");

        //dentro dos resultados apresentados, filtar aqueles que estão entre o peço minimo e maximo
        if (flag_valido)
        {
            for (var j = 0; j < tables_cols.length; j++) {
                var nomefaceta = tables_cols[j].getAttribute("nome_faceta");
                var n_col = document.createElement('TD');
                var n_col_text = document.createTextNode(anuncio[nomefaceta]);

              //caso seja Fotografia
                if (tiposfacetas[j].tipo == 'url') {
                    var link = anuncio[nomefaceta].toString().split(',');

                    //caso tenha mais do que uma fotografia
                    if (link.length > 1) {
                        for (var k = 0; k < link.length; k++) {
                            var img = document.createElement('img');
                            img.setAttribute('src', link[k]);
                            n_col.appendChild(img);
                            var espaco = document.createTextNode(" ");
                            n_col.appendChild(espaco);
                        }
                        n_row.appendChild(n_col);
                    }
                    else {
                        var img = document.createElement('img');
                        img.setAttribute('src', anuncio[nomefaceta]);
                        n_col.appendChild(img);
                        n_row.appendChild(n_col);
                        link = [];
                    }
                } else {

                    n_col.appendChild(n_col_text);
                    n_row.appendChild(n_col);

                }
            }

            tabelares.appendChild(n_row);

        }
    }
}

//Função que "recolhe" os valores das checkbox selecionadas e poe numa variavel para fazer chamada a API
function actualiza_procura()
{
    var search_params = "";
    //div_filters
    var main_filters = document.getElementById("div_filters");
    var all_filters = main_filters.getElementsByTagName("div");
    //Percorrer todos os tipos de filtro
    for (var i = 0; i < all_filters.length; i++) {
        //Percorrer todos as opçoes de cada tipo [input's]
        var all_options = all_filters[i].getElementsByTagName("input");

        var tmp_options = "";
        for (var k = 0; k < all_options.length; k++) {
            if (all_options[k].checked) {
                //para permitir multiplos
                if (tmp_options != "") {
                    tmp_options += ",";
                }
                tmp_options += all_options[k].name;

            }
        }
        //concatenar string dos varios valores de facetas selecionados Ex: tipo_imovel=apartamento&tipo_anuncio=compra
        if (search_params != "" && tmp_options != "") {
            search_params += "&";
        }

        //preencher string para chamada a API
        if (tmp_options != "") {
            var tfaceta = all_filters[i].getAttribute("nome_faceta");
            search_params = search_params + tfaceta + "=" + tmp_options;

        }
    }
    //chama metodo para fazer chamada a API
    if (search_params != "") {
        getanuncioAJAX(search_params);
    }
    else {
        // limpar tbody
        limpeza("search_results_body");
    }
}


function limpeza(elemento) {
    var aux_elem = document.getElementById(elemento);
    while (aux_elem.firstChild)
    {
        aux_elem.removeChild(aux_elem.firstChild);
    }

}