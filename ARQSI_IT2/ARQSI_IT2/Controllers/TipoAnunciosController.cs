﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ARQSI_IT2.ClassLibrary.Models;
using ARQSI_IT2.ClassLibrary.Models;
using ClassLibrary;


namespace ARQSI_IT2.Controllers
{
    public class TipoAnunciosController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: TipoAnuncios
        public ActionResult Index()
        {
            return View(db.TipoAnuncios.ToList());
        }

        // GET: TipoAnuncios/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoAnuncio tipoAnuncio = db.TipoAnuncios.Find(id);
            if (tipoAnuncio == null)
            {
                return HttpNotFound();
            }
            return View(tipoAnuncio);
        }

        // GET: TipoAnuncios/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TipoAnuncios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,tipoanuncio")] TipoAnuncio tipoAnuncio)
        {
            if (ModelState.IsValid)
            {
                db.TipoAnuncios.Add(tipoAnuncio);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tipoAnuncio);
        }

        // GET: TipoAnuncios/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoAnuncio tipoAnuncio = db.TipoAnuncios.Find(id);
            if (tipoAnuncio == null)
            {
                return HttpNotFound();
            }
            return View(tipoAnuncio);
        }

        // POST: TipoAnuncios/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,tipoanuncio")] TipoAnuncio tipoAnuncio)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipoAnuncio).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tipoAnuncio);
        }

        // GET: TipoAnuncios/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoAnuncio tipoAnuncio = db.TipoAnuncios.Find(id);
            if (tipoAnuncio == null)
            {
                return HttpNotFound();
            }
            return View(tipoAnuncio);
        }

        // POST: TipoAnuncios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TipoAnuncio tipoAnuncio = db.TipoAnuncios.Find(id);
            db.TipoAnuncios.Remove(tipoAnuncio);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
