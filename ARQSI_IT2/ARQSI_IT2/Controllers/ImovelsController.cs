﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ARQSI_IT2.ClassLibrary.Models;
using System.Web.UI;

using ARQSI_IT2.ClassLibrary.Models;
using ClassLibrary;

namespace ARQSI_IT2.Controllers
{
    public class ImovelsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        //AnuncioDBContext dbb = new AnuncioDBContext();

        // GET: Imovels
        public ActionResult Index()
        {
            var Imoveis = db.Imovels.Include(c => c.tipoimovel).Include(c => c.localização).Include(c => c.fotos);
            return View(Imoveis.ToList());
        }

        private void PopularTipoImovelDropDownList(object tiposeleccionado = null)
        {
            var tipoimovelQuery = from t in db.TipoImovels
                                  orderby t.tipo
                                  select t;

            ViewBag.Tipo_ImovelID = new SelectList(tipoimovelQuery, "ID", "TIPO", tiposeleccionado);
        }

        // GET: Imovels/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Imovel imovel = db.Imovels.Find(id);
            if (imovel == null)
            {
                return HttpNotFound();
            }
            return View(imovel);
        }

        // GET: Imovels/Create
        public ActionResult Create()
        {
           
            PopularTipoImovelDropDownList();
            return View();
        }

        // POST: Imovels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,area,tipo_imovelID,morada,coordenadas,fotos")] Imovel imovel)
        {
            if (ModelState.IsValid)
            {

                //cria um objecto do tipo localização com os valores das textbox e grava no imovel
                Localização local = new Localização() { morada = Request["txtmorada"].ToString(), coordenadas = Request["txtcoordenadas"].ToString() };

                db.Localização.Add(local);

                imovel.localização = local;

                //int x = ViewBag.Tipo_ImovelID.SelectedValue;
                //imovel.tipo_imovelID = ViewBag.Tipo_ImovelID.SelectedValue;

                db.Imovels.Add(imovel);

                db.SaveChanges();
                return RedirectToAction("Index");
            }

                
                PopularTipoImovelDropDownList(imovel.tipo_imovelID);
           
            
            return View(imovel);
        }

        // GET: Imovels/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Imovel imovel = db.Imovels.Find(id);
            if (imovel == null)
            {
                return HttpNotFound();
            }
            PopularTipoImovelDropDownList(imovel.tipo_imovelID);
            return View(imovel);
        }

        // POST: Imovels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,area,tipo_imovelID,morada,coordenadas,fotos")] Imovel imovel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(imovel).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            PopularTipoImovelDropDownList(imovel.tipo_imovelID);
            return View(imovel);
        }

        // GET: Imovels/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Imovel imovel = db.Imovels.Find(id);
            if (imovel == null)
            {
                return HttpNotFound();
            }
            return View(imovel);
        }

        // POST: Imovels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Imovel imovel = db.Imovels.Find(id);
            db.Imovels.Remove(imovel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
