﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace ARQSI_IT2.ClassLibrary.Models
{
    public class Utilizador : IdentityUser
    {
        //public int ID { get; set; }

        //[Required(ErrorMessage = "Introduza um username", AllowEmptyStrings = false)]
        //[Display(Name ="Username")]
        //public string Username { get; set; }

        [Required(ErrorMessage = "Introduza o primeiro e último nome", AllowEmptyStrings = false)]
        [Display(Name = "Nome Completo")]
        public string NomeCompleto { get; set; }

        //[Required(ErrorMessage = "Introduza o e-mail", AllowEmptyStrings = false)]
        //[DataType(DataType.EmailAddress)]
        //[EmailAddress]
        //public string Email { get; set; }

        [Required(ErrorMessage = "Introduza uma password", AllowEmptyStrings = false)]
        [DataType(System.ComponentModel.DataAnnotations.DataType.Password)]
        [StringLength(50, MinimumLength = 8, ErrorMessage = "Introduza uma password entre 8 e 50 caractéres")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirme a password", AllowEmptyStrings = false)]
        [DataType(System.ComponentModel.DataAnnotations.DataType.Password)]
        [Display(Name = "Confirme a Password")]
        [Compare("Password",ErrorMessage = "As passwords não são iguais")]
        public string ConfirmaPassword { get; set; }

        public TipoUtilizador TipoUtilizador { get; set; }
        public IdentityRole Role { get; set; }
    }

    public class TipoUtilizador 
    {
        public int ID { get; set; }
        public string Descrição { get; set; }
    }

    public class Login
    {
        public int ID { get; set; }

        [Required]
        [Display(Name = "Username")]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class VerifyCode
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

}