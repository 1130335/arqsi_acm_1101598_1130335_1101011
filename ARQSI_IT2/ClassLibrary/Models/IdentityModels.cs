﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ARQSI_IT2.ClassLibrary.Models;


namespace ClassLibrary
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public string NomeCompleto { get; set; }
        public TipoUtilizador TipoUtilizador { get; set; }

    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<ARQSI_IT2.ClassLibrary.Models.Utilizador> Utilizadors { get; set; }
        public System.Data.Entity.DbSet<ARQSI_IT2.ClassLibrary.Models.TipoUtilizador> TipoUtilizadors { get; set; }
        public System.Data.Entity.DbSet<ARQSI_IT2.ClassLibrary.Models.Imovel> Imovels { get; set; }
        public System.Data.Entity.DbSet<ARQSI_IT2.ClassLibrary.Models.TipoImovel> TipoImovels { get; set; }
        public System.Data.Entity.DbSet<ARQSI_IT2.ClassLibrary.Models.Anuncio> Anuncios { get; set; }
        public System.Data.Entity.DbSet<ARQSI_IT2.ClassLibrary.Models.TipoAnuncio> TipoAnuncios { get; set; }
        public System.Data.Entity.DbSet<ARQSI_IT2.ClassLibrary.Models.Fotos> Fotos { get; set; }
        public System.Data.Entity.DbSet<ARQSI_IT2.ClassLibrary.Models.Localização> Localização { get; set; }

        public System.Data.Entity.DbSet<ARQSI_IT2.ClassLibrary.Models.Login> Logins { get; set; }

        
    }
}