﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ARQSI_IT2.ClassLibrary.Models;



namespace ARQSI_IT2.ClassLibrary.Models
{
    public class TipoImovel
    {
        public int ID { get; set; }
        public string tipo { get; set; }
     


        public virtual ICollection<Imovel> Imoveis { get; set; }
    }
    }
