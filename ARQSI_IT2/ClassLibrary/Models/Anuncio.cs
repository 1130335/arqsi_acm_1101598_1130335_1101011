﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using ARQSI_IT2.ClassLibrary.Models;

namespace ARQSI_IT2.ClassLibrary.Models
{
    public class Anuncio
    {
        public int ID { get; set; }
        public float preco { get; set; }
        public int tipoanuncioID { get; set; }
        public TipoAnuncio tipoanuncio { get; set; }
        public int imovelID { get; set; }
        public Imovel imovel { get; set; }
		public Mediador mediador {get;set;}
    }




}