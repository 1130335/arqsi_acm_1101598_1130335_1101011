﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ARQSI_IT2.ClassLibrary.Models;

namespace ARQSI_IT2.ClassLibrary.Models
{
    public class Imovel
    {
        public int ID { get; set; }
        public int area { get; set; }
        public Fotos fotos { get; set; }
        public Localização localização { get; set; }
        public int tipo_imovelID { get; set; }
        public TipoImovel tipoimovel { get; set; }

        public virtual ICollection<Anuncio> Anuncios { get; set; }
      

    }
}