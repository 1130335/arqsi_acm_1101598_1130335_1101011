<?php
$title = 'Novo Mediador';
$this->headTitle($title);
?>
 <h1><?php echo $this->escapeHtml($title); ?></h1>
 <?php
 $form->setAttribute('action', $this->url('mediador', array('action' => 'add')));
 $form->prepare();

 echo $this->form()->openTag($form);
 echo $this->formHidden($form->get('id'));
 echo $this->formRow($form->get('username'));
 echo $this->formRow($form->get('password'));
 echo $this->formSubmit($form->get('submit'));
 echo $this->form()->closeTag();