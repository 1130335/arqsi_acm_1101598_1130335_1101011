<?php
 namespace ARQSI_IT3_Mediador\Form;

 use Zend\Form\Form;

 class ConfigForm extends Form
 {
     public function __construct($name = null)
     {
         // we want to ignore the name passed
         parent::__construct('server');

         $this->add(array(
             'name' => 'server',
             'type' => 'Text',
             'options' => array(
                 'label' => 'Nome/IP: https://',
             ),
         ));
         $this->add(array(
             'name' => 'submit',
             'type' => 'Submit',
             'attributes' => array(
                 'value' => 'Go',
                 'id' => 'submitbutton',
             ),
         ));
     }
 }
?>