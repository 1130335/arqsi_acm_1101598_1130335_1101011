<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Mediador for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace ARQSI_IT3_Mediador\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use ARQSI_IT3_Mediador\Form\LoginForm;
use ARQSI_IT3_Mediador\DTO\Credenciais;
use ARQSI_IT3_Mediador\Infrastructure\Services\ImoServices;
use Zend\View\Helper\ViewModel;
use ARQSI_IT3_Mediador\Form\MediadorForm;
use ARQSI_IT3_Mediador\Model\Model_Mediador;

class MediadorController extends AbstractActionController
{
    public function indexAction()
    {
        return new ViewModel();
    }

    public function fooAction()
    {
        // This shows the :controller and :action parameters in default route
        // are working when you browse to /mediador/mediador/foo
        return array();
    }
    public function loginAction()
    {
        $form = new LoginForm();
        $form->get('submit')->setValue('Login');
    
        $request = $this->getRequest();
        if ($request->isPost()) {
    
            session_start();
            ImoServices::Logout();
    
            $credenciais = new Credenciais();
            $form->setInputFilter($credenciais->getInputFilter());
            $form->setData($request->getPost());
    
            if ($form->isValid()) {
                $credenciais->exchangeArray($form->getData());
                 
                if( ImoServices::Login($credenciais) )
    
                    // Redirect to values
                    return $this->redirect()->toRoute('mediador', array('controller'=>'mediador', 'action' => 'index'));
            }
            
            }
        
    
        
        return array('form' => $form);
            }
            
    
    public function logoutAction()
    {
        ImoServices::Logout();
    
        return $this->redirect()->toRoute('mediador', array('controller'=>'mediador', 'action' => 'login'));
    }
   
    public function addAction()
    {
        $form = new MediadorForm();
        $form->get('submit')->setValue('Add');
    
        $request = $this->getRequest();
        if ($request->isPost()) {
            $credenciais = new Credenciais();
            $form->setInputFilter($credenciais->getInputFilter());
            $form->setData($request->getPost());
    
            if ($form->isValid()) {
                $mediador->exchangeArray($form->getData());
                ImoServices::Add($credenciais);
                  
                return $this->redirect()->toRoute('mediador', array('controller'=>'mediador', 'action' => 'index'));
            }
        }
        return array('form' => $form);
    }
}
