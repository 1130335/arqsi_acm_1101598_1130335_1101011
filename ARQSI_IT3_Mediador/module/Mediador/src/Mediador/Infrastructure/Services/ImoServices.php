<?php
namespace ARQSI_IT3_Mediador\Infrastructure\Services;

use Zend\Http\Client;
use Zend\Http\Request;
use Zend\Json\Json;

use ARQSI_IT3_Mediador\DTO\Credenciais;

class ImoServices
{
    const URL_Login = '/Token';
    const URL_Values = '/api/values';
    const URL_Client= '/api/Account/Register';
    
    public static function Login(Credenciais $credenciais)
    {
        session_start();
        
        //$client = new Client('https://' .$_SESSION['server'] .self::URL_Login);
        $client = new Client('http://localhost:39676'.self::URL_Login);
        
        $client->setMethod(Request::METHOD_POST);
        $params = 'grant_type=password&username=' . $credenciais->username .'&password=' .$credenciais->password;
        
        $len = strlen($params);
        
        $client->setHeaders(array(
            'Content-Type'   => 'application/x-www-form-urlencoded',
            'Content-Length' => $len 
        ));        
        
        $client->setOptions(['sslverifypeer' => false]);
        $client->setRawBody($params);
        
        $response = $client->send();            
        
        $body=Json::decode($response->getBody());
        
        if(!empty($body->access_token))
        {
            session_start();

            $_SESSION['access_token'] = $body->access_token;
            $_SESSION['username'] = $credenciais->username;

            return true;
        }
        else 
            return false;
    }
    
    public static function Logout()
    {
        session_start();
        
        $_SESSION['username'] = null;
        $_SESSION['access_token'] = null;
    }
    
    public static function getValues()
    {
        session_start();

        $client = new Client('http://localhost:39676'.self::URL_Values);
        
        $client->setMethod(Request::METHOD_GET);

        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        
        $client->setHeaders(array(
            'Authorization'   => $bearer_token,
        ));
        
        $client->setOptions(['sslverifypeer' => false]);
        
        $response = $client->send();
        
        $body=$response->getBody();
        
        return $body;
    }
    
    public static function Add(Credenciais $credenciais)
    {
        $client = new Client('http://localhost:39676'.self::URL_Client);
        
        $client->setMethod(Request::METHOD_POST);
        $params = $credenciais->username+$credenciais->password;
        
        $len = strlen($params);
        
        $client->setHeaders(array(
            'Content-Type'   => 'application/x-www-form-urlencoded',
            'Content-Length' => $len
        ));
        
        $client->setOptions(['sslverifypeer' => false]);
        $client->setRawBody($params);
        
        $response = $client->send();
        
        $body=Json::decode($response->getBody());
        
      
        If(!$body->OK)
        {
            return false;
        }
        return true;
    }
    
}

?>