<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Http\Client;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use ARQSI_IT3_Mediador\Controller;

class IndexController extends AbstractActionController
{
    
    public function indexAction()
    {
        
        return new ViewModel();
    }
    public function getDados()
    {
        $client=new Client("http://phpdev2.dei.isep.ipp.pt/~nsilva/imorest/facetas.php",array('strict' => false));
        
               
        $response = $client->send();
        $body=Json::decode($response->getBody());
        return $body;
        
    }
}
