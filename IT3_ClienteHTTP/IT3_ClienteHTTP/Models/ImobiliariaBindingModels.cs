﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IT3_ClienteHTTP.Models
{
    public enum TipoAnuncioBindingModel
    {
        Compra,
        Venda,
        Permuta,
        Aluguer
    }

    public class AnuncioBindingModel
    {
        public int AnuncioID { get; set; }

        [Required]
        [Display(Name = "Utilizador")]
        public ApplicationUser UtilizadorCriador { get; set; }

        [Required]
        [Display(Name = "Imovel")]
        public ImovelBindingModel Imovel { get; set; }

        [Required]
        [Display(Name = "Tipo de Anuncio")]
        public TipoAnuncioBindingModel Tipo { get; set; }

        [Display(Name = "Mediador")]
        public ApplicationUser MediadorResponsavel { get; set; }

    }

    public class ImovelBindingModel
    {
        public int ImovelID { get; set; }

        [Required]
        [Display(Name = "Preço")]
        public float Preco { get; set; }

        [Required]
        [Display(Name = "Area")]
        public float Area { get; set; }

        [Required]
        [Display(Name = "Tipo de Imovel")]
        public TipoImovelBindingModel TipoImovel { get; set; }

        [Required]
        [Display(Name = "Localização")]
        public LocalizacaoBindingModel Localização { get; set; }

        [Display(Name = "Fotos")]
        public FotosBindingModel Fotos { get; set; }

        public virtual ICollection<AnuncioBindingModel> Anuncios { get; set; }
    }

    public class TipoImovelBindingModel
    {
        public int TipoImovelID { get; set; }

        [Required]
        [Display(Name = "Descricao")]
        public string Descricao { get; set; }

        [Display(Name = "Tipo Imovel Adicional")]
        public TipoImovelBindingModel TipoAdicional { get; set; }

        public virtual ICollection<ImovelBindingModel> Imoveis { get; set; }
    }

    public class LocalizacaoBindingModel
    {
        public int LocalizacaoID { get; set; }

        [Required]
        [Display(Name = "Morada")]
        public string Morada { get; set; }

        [Display(Name = "Coordenadas")]
        public string Coordenadas { get; set; }
    }

    public class FotosBindingModel
    {
        public int FotosID { get; set; }

        [Display(Name = "URL")]
        public string URL { get; set; }
    }

    public class AlertaBindingModel
    {
        public int AlertaID { get; set; }

        [Required]
        [Display(Name = "Utilizador Criador")]
        public ApplicationUser UtilizadorCriador { get; set; }

        [Required]
        [Display(Name = "Parametros")]
        public string Parametros { get; set; }
    }
}