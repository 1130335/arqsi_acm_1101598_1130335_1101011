﻿using IT3_ClienteHTTP.Helpers;
using IT3_ClienteHTTP.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace IT3_ClienteHTTP.Controllers
{
    public class AnuncioController : Controller
    {
        public AnuncioController()
        {

        }

        // GET: Anuncio
        public async Task<ActionResult> Index()
        {
            
            var client = WebAPI_ClienteHTTP.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Anuncio");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var anuncios = JsonConvert.DeserializeObject<IEnumerable<AnuncioBindingModel>>(content);
                return View(anuncios);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        // GET: Anuncio/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebAPI_ClienteHTTP.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Anuncio/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var anuncio = JsonConvert.DeserializeObject<AnuncioBindingModel>(content);
                if (anuncio == null) return HttpNotFound();
                return View(anuncio);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        // GET: Anuncio/Create
        public ActionResult Create()
        {
            
            return View();
        }

        // POST: Anuncio/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,Preco,tipoanuncio,ImovelID,Mediador")] AnuncioBindingModel anuncio)
        {
            try
            {
                var client = WebAPI_ClienteHTTP.GetClient();
                string JSON = JsonConvert.SerializeObject(anuncio);
                HttpContent content = new StringContent(JSON,
                    System.Text.Encoding.Unicode, "application/json");
                var response = await client.PostAsync("api/Anuncio", content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
            
        }

        // GET: Anuncio/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebAPI_ClienteHTTP.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Anuncio/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var anuncio = JsonConvert.DeserializeObject<AnuncioBindingModel>(content);
                if (anuncio == null) return HttpNotFound();
                return View(anuncio);
            }
            return Content("Ocorreu um erro: " + response.StatusCode);
            
        }

        // POST: Anuncio/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Preco,tipoanuncio,ImovelID,Mediador")] AnuncioBindingModel anuncio)
        {
            try
            {
                var client = WebAPI_ClienteHTTP.GetClient();
                string JSON = JsonConvert.SerializeObject(anuncio);
                HttpContent content = new StringContent(JSON,
                    System.Text.Encoding.Unicode, "application/json");
                var response = await client.PutAsync("api/Anuncio/" + anuncio.AnuncioID, content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
            //ViewBag.ImovelID = new SelectList(db.Imovels, "ID", "ID", anuncio.ImovelID);
            //return View(anuncio);
        }

        // GET: Anuncio/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebAPI_ClienteHTTP.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Anuncio/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var anuncio = JsonConvert.DeserializeObject<AnuncioBindingModel>(content);
                if (anuncio == null) return HttpNotFound();
                return View(anuncio);
            }
            return Content("Ocorreu um erro: " + response.StatusCode);
        }

        // POST: Anuncio/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var client = WebAPI_ClienteHTTP.GetClient();
                var response = await client.DeleteAsync("api/Anuncio/" + id);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }
    }
}