﻿using System.Web;
using System.Web.Mvc;

namespace IT3_ClienteHTTP
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
