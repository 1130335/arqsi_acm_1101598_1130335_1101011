﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(IT3_ClienteHTTP.Startup))]
namespace IT3_ClienteHTTP
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
